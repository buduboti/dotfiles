
alias c="clear"
alias q="exit"
alias :q="exit"

alias lg="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"

alias ls="ls --color"
alias l="ls -l --color"
alias ll="ls -la --color"

alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."

alias dot="cd $DOTFILES"

alias grep="grep --color=always"

alias path="echo $PATH | tr ':' '\n'"

alias weather="curl wttr.in"

# change directory aliases
# source "$ZSH/cda.zsh"
