require("nvim-lsp-installer").setup({
    automatic_installation = true, -- automatically detect which servers to install (based on which servers are set up via lspconfig)
    ui = {
        icons = {
            server_installed = "✓",
            server_pending = "➜",
            server_uninstalled = "✗"
        }
    }
})

local lsp = require('lspconfig')

-- local map = function(type, key, value)
-- 	vim.fn.nvim_buf_set_keymap(0,type,key,value,{noremap = true, silent = true});
-- end

-- local custom_attach = function(client)
-- 	require'completion'.on_attach(client)
--
-- 	map('n','gD','<cmd>lua vim.lsp.buf.declaration()<CR>')
-- 	map('n','gd','<cmd>lua vim.lsp.buf.definition()<CR>')
-- 	map('n','gk','<cmd>lua vim.lsp.buf.hover()<CR>')
-- 	map('n','gr','<cmd>lua vim.lsp.buf.references()<CR>')
-- 	map('n','gs','<cmd>lua vim.lsp.buf.signature_help()<CR>')
-- 	map('n','gi','<cmd>lua vim.lsp.buf.implementation()<CR>')
-- 	map('n','gt','<cmd>lua vim.lsp.buf.type_definition()<CR>')
-- 	map('n','<leader>gw','<cmd>lua vim.lsp.buf.document_symbol()<CR>')
-- 	map('n','<leader>gW','<cmd>lua vim.lsp.buf.workspace_symbol()<CR>')
-- 	map('n','<leader>ah','<cmd>lua vim.lsp.buf.hover()<CR>')
-- 	map('n','<leader>af','<cmd>lua vim.lsp.buf.code_action()<CR>')
-- 	map('n','<leader>ee','<cmd>lua vim.lsp.util.show_line_diagnostics()<CR>')
-- 	map('n','<leader>ar','<cmd>lua vim.lsp.buf.rename()<CR>')
-- 	map('n','<leader>=', '<cmd>lua vim.lsp.buf.formatting()<CR>')
-- 	map('n','<leader>ai','<cmd>lua vim.lsp.buf.incoming_calls()<CR>')
-- 	map('n','<leader>ao','<cmd>lua vim.lsp.buf.outgoing_calls()<CR>')
-- end

lsp.tsserver.setup{on_attach=custom_attach}
lsp.cmake.setup{on_attach=custom_attach}
lsp.clangd.setup{on_attach=custom_attach}
lsp.bashls.setup{on_attach=custom_attach}
lsp.pylsp.setup{on_attach=custom_attach}
lsp.vimls.setup{on_attach=custom_attach}
lsp.gopls.setup{on_attach=custom_attach}
lsp.sourcekit.setup{on_attach=custom_attach}

-- lsp.efm.setup {
--     init_options = {documentFormatting = true},
--     settings = {
--         rootMarkers = {".git/"},
--         languages = {
--             lua = {
--                 {formatCommand = "lua-format -i", formatStdin = true}
--             }
--         }
--     }
-- }

